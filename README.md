# exopipes

ship data from silo to silo

## abstract

pinboard doesn't provide an option to export, except through their api. raindrop provides import options, but not one that integrates with pinboard's api. there is [a python script on github](https://github.com/karlicoss/pinbexport) that exports your pinboard bookmarks, but it's not very easy to use and there's no explanation of what format they are exported in or whether it's one that Raindrop could import. so this tool would read from pinboard's api and write to raindrop's api and do the transfer directly.

with a plug in architecture this could be generalized to a tool that imports and exports between lots of different sites, kind of like an rclone for structured data that's locked up in silos.

it sucks that such a thing even has to exist but well here we are.

## other possible pipes

- migrating/syncing issue trackers
  - e.g. gitlab issues <-> youtrack
- todo list apps
- contacts (?)
- emails (?)
- sync (versioned) intermediate data structures to git repo